package pt.vianasports.services;


import pt.vianasports.daos.CategoryDao;
import pt.vianasports.daos.ClientDao;
import pt.vianasports.models.Category;
import pt.vianasports.models.Client;
import pt.vianasports.models.Employee;

import java.util.Date;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this tclientlate file, choose Tools | Tclientlates
 * and open the tclientlate in the editor.
 */

/**
 *
 * @author pedro
 */
public class CategoryService {
    private CategoryDao categoryDao;

    public CategoryService() {
        categoryDao = new CategoryDao();
    }
    
    public List<Category> getAll() {
        return categoryDao.getAll();
    }

    public boolean create(Category category) {
        return categoryDao.persist(category);
    }
}
