package pt.vianasports.services;


import java.util.Date;
import java.util.List;
import pt.vianasports.daos.EmployeeDao;
import pt.vianasports.models.Employee;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pedro
 */
public class EmployeeService {
    private EmployeeDao empDao;

    public EmployeeService() {
        empDao = new EmployeeDao();
    }

    public Employee authenticate(String username, String password) {
        Employee emp = empDao.getByCredentials(username, password);
        return emp;
    }
    
    public List<Employee> getAll() {
        return empDao.getAll();
    }

    public boolean create(Employee emp) {
        emp.setCreationDate(new Date());
        return empDao.persist(emp);
    }

    public boolean update(Employee emp, Employee updater) {
        emp.setLastUpdate(new Date());
        emp.setUpdater(updater);
        return empDao.update(emp);
    }

    public boolean delete(Employee emp) {
        return empDao.delete(emp);
    }
}
