package pt.vianasports.services;


import pt.vianasports.daos.ClientDao;
import pt.vianasports.daos.ProductDao;
import pt.vianasports.models.Client;
import pt.vianasports.models.Employee;
import pt.vianasports.models.Product;

import javax.swing.*;
import java.util.Date;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this tclientlate file, choose Tools | Tclientlates
 * and open the tclientlate in the editor.
 */

/**
 *
 * @author pedro
 */
public class ProductService {
    private ProductDao productDao;

    public ProductService() {
        productDao = new ProductDao();
    }
    
    public List<Product> getAll() {
        return productDao.getAll();
    }

    public boolean create(Product product, Employee creator) {
        product.setCreationDate(new Date());
        product.setCreator(creator);
        return productDao.persist(product);
    }

    public Product getById(long id) {
        return productDao.getById(id);
    }

    public boolean update(Product product, Employee updater) {
        product.setLastUpdateDate(new Date());
        product.setUpdater(updater);
        return productDao.update(product);
    }

    public boolean delete(Product product) {
        return productDao.delete(product);
    }
}
