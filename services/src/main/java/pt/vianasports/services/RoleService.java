package pt.vianasports.services;

import pt.vianasports.daos.EmployeeDao;
import pt.vianasports.daos.RoleDao;
import pt.vianasports.models.Role;

import java.util.List;

public class RoleService {
    private RoleDao roleDao;

    public RoleService() {
        roleDao = new RoleDao();
    }

    public List<Role> getAll() {
        return roleDao.getAll();
    }
}
