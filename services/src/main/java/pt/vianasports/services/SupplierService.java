package pt.vianasports.services;


import pt.vianasports.daos.SupplierDao;
import pt.vianasports.models.Supplier;
import pt.vianasports.models.Employee;

import java.util.Date;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this tsupplierlate file, choose Tools | Tsupplierlates
 * and open the tsupplierlate in the editor.
 */

/**
 *
 * @author pedro
 */
public class SupplierService {
    private SupplierDao supplierDao;

    public SupplierService() {
        supplierDao = new SupplierDao();
    }
    
    public List<Supplier> getAll() {
        return supplierDao.getAll();
    }

    public boolean create(Supplier supplier) {
        supplier.setCreationDate(new Date());
        return supplierDao.persist(supplier);
    }

    public boolean update(Supplier supplier, Employee updater) {
        supplier.setLastUpdate(new Date());
        supplier.setUpdater(updater);
        return supplierDao.update(supplier);
    }

    public boolean delete(Supplier supplier) {
        return supplierDao.delete(supplier);
    }
}
