package pt.vianasports.services;


import pt.vianasports.daos.ClientDao;
import pt.vianasports.models.Client;
import pt.vianasports.models.Employee;

import java.util.Date;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this tclientlate file, choose Tools | Tclientlates
 * and open the tclientlate in the editor.
 */

/**
 *
 * @author pedro
 */
public class ClientService {
    private ClientDao clientDao;

    public ClientService() {
        clientDao = new ClientDao();
    }
    
    public List<Client> getAll() {
        return clientDao.getAll();
    }

    public boolean create(Client client) {
        client.setCreationDate(new Date());
        return clientDao.persist(client);
    }

    public boolean update(Client client, Employee updater) {
        client.setLastUpdate(new Date());
        client.setUpdater(updater);
        return clientDao.update(client);
    }

    public boolean delete(Client client) {
        return clientDao.delete(client);
    }
}
