package pt.vianasports.services;

import pt.vianasports.daos.SaleDao;
import pt.vianasports.models.Employee;
import pt.vianasports.models.Sale;

import java.util.Date;
import java.util.List;

public class SaleService {
    private SaleDao saleDao;

    public SaleService() {
        saleDao = new SaleDao();
    }

    public List<Sale> getAll() {
        return saleDao.getAll();
    }

    public boolean create(Sale sale) {
        sale.setCreationDate(new Date());
        return saleDao.persist(sale);
    }

    public boolean update(Sale sale, Employee updater) {
        sale.setLastUpdate(new Date());
        sale.setUpdater(updater);
        return saleDao.update(sale);
    }

    public boolean delete(Sale sale) {
        return saleDao.delete(sale);
    }
}
