/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.vianasports.daos;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import pt.vianasports.models.Employee;
import pt.vianasports.utils.HibernateUtil;

/**
 *
 * @author pedro
 */
public class EmployeeDao {
    public Employee getByCredentials(String email, String passwordHash) {
        Session session= null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Employee where email=:email and password=:password");
            query.setParameter("email", email);
            query.setParameter("password", passwordHash);
            List results = query.getResultList();
            
            if (results.isEmpty()) {
                return null;
            } else {
                return (Employee) results.get(0);
            }
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        session.close();
        return null;
    }
    
    public boolean persist(Employee emp) {
        Session session = null;
         try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.persist(emp);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }

    public boolean update(Employee emp) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(emp);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }

    public boolean delete(Employee emp) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(emp);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }
    
    public List<Employee> getAll() {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            List<Employee> result = session.createQuery("FROM Employee emp join fetch emp.role role").getResultList();
            session.close();
            return result;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if(session != null) {
            session.close();
        }
        return new ArrayList<>();
    }
}
