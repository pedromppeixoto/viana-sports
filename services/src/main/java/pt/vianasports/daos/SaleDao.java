package pt.vianasports.daos;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import pt.vianasports.models.Sale;
import pt.vianasports.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class SaleDao {
    public boolean persist(Sale sale) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.persist(sale);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }

    public boolean update(Sale sale) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(sale);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }

    public boolean delete(Sale sale) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(sale);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }

    public List<Sale> getAll() {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            List<Sale> result = session.createQuery("FROM Sale").getResultList();
            session.close();
            return result;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if(session != null) {
            session.close();
        }
        return new ArrayList<>();
    }
}
