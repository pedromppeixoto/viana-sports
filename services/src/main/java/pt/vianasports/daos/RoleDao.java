package pt.vianasports.daos;

import java.util.ArrayList;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pt.vianasports.models.Role;
import pt.vianasports.utils.HibernateUtil;

import java.util.List;

public class RoleDao {

    public void persist(Role entity) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.save(entity);
            transaction.commit();
            session.close();
        } catch (HibernateException ex) {
            throw ex;
        }
    }


    public Role getById(long id) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Role role = session.get(Role.class, id);
            session.close();
            return role;
        } catch (HibernateException ex) {
            throw ex;
        }
    }

    public List<Role> getAll() {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            List<Role> result = session.createQuery("FROM Role").getResultList();
            session.close();
            return result;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if(session != null) {
            session.close();
        }
        return new ArrayList<>();
    }
}
