/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this tsupplierlate file, choose Tools | Tsupplierlates
 * and open the tsupplierlate in the editor.
 */
package pt.vianasports.daos;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import pt.vianasports.models.Supplier;
import pt.vianasports.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pedro
 */
public class SupplierDao {
    
    public boolean persist(Supplier supplier) {
        Session session = null;
         try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.persist(supplier);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }

    public boolean update(Supplier supplier) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(supplier);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }

    public boolean delete(Supplier supplier) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(supplier);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }
    
    public List<Supplier> getAll() {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            List<Supplier> result = session.createQuery("FROM Supplier").getResultList();
            session.close();
            return result;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if(session != null) {
            session.close();
        }
        return new ArrayList<>();
    }
}
