/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.vianasports.daos;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import pt.vianasports.models.Client;
import pt.vianasports.utils.HibernateUtil;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pedro
 */
public class ClientDao {
    
    public boolean persist(Client emp) {
        Session session = null;
         try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.persist(emp);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }

    public boolean update(Client emp) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(emp);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }

    public boolean delete(Client emp) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(emp);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }
    
    public List<Client> getAll() {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            List<Client> result = session.createQuery("FROM Client").getResultList();
            session.close();
            return result;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if(session != null) {
            session.close();
        }
        return new ArrayList<>();
    }
}
