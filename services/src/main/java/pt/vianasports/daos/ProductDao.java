/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.vianasports.daos;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import pt.vianasports.models.Category;
import pt.vianasports.models.Product;
import pt.vianasports.models.ProductSize;
import pt.vianasports.models.Role;
import pt.vianasports.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author pedro
 */
public class ProductDao {
    
    public boolean persist(Product product) {
        Session session = null;
         try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

             Set<Category> attachedCategories = new HashSet();
             for (Category category:
                     product.getCategories()) {

                 if (category.getId() == 0) {
                     session.persist(category);
                 }
                 attachedCategories.add(session.find(Category.class, category.getId()));
             }
             product.setCategories(attachedCategories);

             session.persist(product);

             for (ProductSize ps: product.getSizes()) {
                 ps.setProduct(product);
                 session.persist(ps);
             }
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }

    public boolean update(Product product) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(product);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }

    public boolean delete(Product product) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.createSQLQuery("DELETE FROM ProductCategory WHERE product_id=:param1")
                .setParameter("param1", product.getId())
                .executeUpdate();
            session.delete(product);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }
    
    public List<Product> getAll() {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            List<Product> result = session.createQuery("SELECT DISTINCT prod FROM Product prod " +
                    "join fetch prod.sizes " +
                    "join fetch prod.categories").getResultList();
            session.close();
            return result;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if(session != null) {
            session.close();
        }
        return new ArrayList<>();
    }

    public Product getById(long id) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("SELECT prod FROM Product prod " +
                    "join fetch prod.sizes " +
                    "join fetch prod.categories " +
                    "WHERE prod.id = :id");
            query.setParameter("id", id);
            Product product = (Product) query.getResultList().get(0);
            session.close();
            return product;
        } catch (HibernateException ex) {
            throw ex;
        }
    }
}
