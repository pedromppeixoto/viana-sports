/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.vianasports.daos;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import pt.vianasports.models.Category;
import pt.vianasports.models.Client;
import pt.vianasports.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pedro
 */
public class CategoryDao {
    
    public boolean persist(Category category) {
        Session session = null;
         try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.persist(category);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if (session != null) {
            session.close();
        }
        return false;
    }

    public List<Category> getAll() {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            List<Category> result = session.createQuery("FROM Category").getResultList();
            session.close();
            return result;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if(session != null) {
            session.close();
        }
        return new ArrayList<>();
    }
}
