package pt.vianasports.utils;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    public static SessionFactory factory;

    private HibernateUtil() {
    }

    public static synchronized SessionFactory getSessionFactory() {
        
        if (factory == null) {
            try {
                 factory = new Configuration().configure("hibernate.cfg.xml").
                    buildSessionFactory();
            } catch (HibernateException e) {
                throw e;
            }
           
        }
        return factory;
    }
}
