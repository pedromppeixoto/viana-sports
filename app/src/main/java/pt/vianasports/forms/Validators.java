package pt.vianasports.forms;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validators {

    public static boolean isNif(String value) {
        final int max=9;
        //check if is numeric and has 9 numbers
        if (!value.matches("[0-9]+") || value.length()!=max) return false;
        int checkSum=0;
        //calculate checkSum
        for (int i=0; i<max-1; i++){
            checkSum+=(value.charAt(i)-'0')*(max-i);
        }
        int checkDigit=11-(checkSum % 11);
        //if checkDigit is higher than TEN set it to zero
        if (checkDigit>=10) checkDigit=0;
        //compare checkDigit with the last number of NIF
        return checkDigit==value.charAt(max-1)-'0';
    }

    public static boolean isEmail(String value) {
        String regex = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
        return value.matches(regex);
    }

    public static boolean isPhoneNumber(String value) {
        String patterns
                = "^(\\+\\d{1,3}( )?)?((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$"
                + "|^(\\+\\d{1,3}( )?)?(\\d{3}[ ]?){2}\\d{3}$"
                + "|^(\\+\\d{1,3}( )?)?(\\d{3}[ ]?)(\\d{2}[ ]?){2}\\d{2}$";
        Pattern pattern = Pattern.compile(patterns);
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }
}
