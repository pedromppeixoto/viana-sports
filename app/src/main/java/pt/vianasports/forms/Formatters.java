package pt.vianasports.forms;

import javafx.scene.control.TextFormatter;
import javafx.util.converter.IntegerStringConverter;

import javax.persistence.criteria.CriteriaBuilder;
import java.text.DecimalFormat;
import java.text.ParsePosition;
import java.util.function.UnaryOperator;

public class Formatters {
    public static TextFormatter decimalFormatter() {
        DecimalFormat format = new DecimalFormat( "#.0" );
        return new TextFormatter<>(c ->
        {
            if ( c.getControlNewText().isEmpty() )
            {
                return c;
            }

            ParsePosition parsePosition = new ParsePosition( 0 );
            Object object = format.parse( c.getControlNewText(), parsePosition );

            if ( object == null || parsePosition.getIndex() < c.getControlNewText().length() )
            {
                return null;
            }
            else
            {
                return c;
            }
        });
    }

    public static TextFormatter intFormatter() {
        UnaryOperator<TextFormatter.Change> integerFilter = change -> {
            String newText = change.getControlNewText();
            if (newText.matches("-?([1-9][0-9]*)?")) {
                return change;
            }
            return null;
        };
        return new TextFormatter(integerFilter);
    }
}
