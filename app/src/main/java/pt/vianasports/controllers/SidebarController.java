/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.vianasports.controllers;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import pt.vianasports.controllers.client.ClientScreenController;
import pt.vianasports.controllers.orders.OrdersScreenController;
import pt.vianasports.controllers.product.ProductsScreenController;
import pt.vianasports.controllers.sales.SalesScreenController;
import pt.vianasports.controllers.supplier.SupplierScreenController;
import pt.vianasports.controllers.user.UserScreenController;

/**
 *
 * @author pedro
 */
public class SidebarController extends GenericController{
    @FXML
    private void loadHome(ActionEvent event) throws IOException {
        MainScreenController mainScreenController = new MainScreenController();
        context.loadScreen(getClass().getResource("/fxml/mainScreen.fxml"), mainScreenController);
    }
 
    @FXML
    private void loadUsersScreen(ActionEvent event) throws IOException {
        this.context.currentPageTitle = "Utilizadores";
        UserScreenController controller = new UserScreenController();
        context.loadScreen(getClass().getResource("/fxml/user/usersScreen.fxml"), controller);
    }

    @FXML
    private void loadClientScreen(ActionEvent event) throws IOException {
        this.context.currentPageTitle = "Clientes";
        ClientScreenController controller = new ClientScreenController();
        context.loadScreen(getClass().getResource("/fxml/clients/clientScreen.fxml"), controller);
    }

    @FXML
    private void loadSupplierScreen(ActionEvent event) throws IOException {
        this.context.currentPageTitle = "Fornecedores";
        SupplierScreenController controller = new SupplierScreenController();
        context.loadScreen(getClass().getResource("/fxml/supplier/supplierScreen.fxml"), controller);
    }

    @FXML
    private void loadProductsScreen(ActionEvent event) throws IOException {
        this.context.currentPageTitle = "Produtos";
        ProductsScreenController controller = new ProductsScreenController();
        context.loadScreen(getClass().getResource("/fxml/product/productsScreen.fxml"), controller);
    }

    @FXML
    private void loadSalesScreen(ActionEvent event) throws IOException {
        this.context.currentPageTitle = "Vendas";
        SalesScreenController controller = new SalesScreenController();
        context.loadScreen(getClass().getResource("/fxml/sales/salesScreen.fxml"), controller);
    }

    @FXML
    private void loadOrdersScreen(ActionEvent event) throws IOException {
        this.context.currentPageTitle = "Encomendas";
        OrdersScreenController controller = new OrdersScreenController();
        context.loadScreen(getClass().getResource("/fxml/orders/ordersScreen.fxml"), controller);
    }
}
