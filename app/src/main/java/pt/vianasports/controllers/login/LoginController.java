/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.vianasports.controllers.login;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.PasswordField;
import pt.vianasports.controllers.GenericController;
import pt.vianasports.controllers.MainScreenController;
import pt.vianasports.services.EmployeeService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import pt.vianasports.models.Employee;

/**
 *
 * @author pedro
 */
public class LoginController extends GenericController {
    private EmployeeService empService;

    @FXML
    private ResourceBundle resources;
    
    @FXML
    private GridPane loginForm;
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label errorLabel;
    @FXML
    private ProgressIndicator progressIndicator;
    
    
    @FXML
    public void initialize() { }
    
    private void authencticationResult(Employee employee) throws IOException {
        progressIndicator.setVisible(false);
        loginForm.setDisable(false);
        if (employee == null) {
             errorLabel.setText(resources.getString("invalidCredentials"));
        } else {
            this.context.loggedUser = employee;
            this.context.loadScreen(getClass().getResource("/fxml/mainScreen.fxml"), new MainScreenController());
        }
    }
    
    @FXML
    private void login(ActionEvent event) {
        errorLabel.setText("");
        loginForm.setDisable(true);
        progressIndicator.setVisible(true);

        if (usernameField.getText().isEmpty()) {
        }
        
        new Thread(new Runnable() {
            @Override
            public void run() {
                final Employee employee = context.empService.authenticate(usernameField.getText(), passwordField.getText());
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            authencticationResult(employee);
                        } catch (IOException ex) {
                            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
            }
        }).start();
    } 
    
    
}
