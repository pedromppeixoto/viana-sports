/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.vianasports.controllers;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author pedro
 */
public class MainScreenController extends GenericController{
    @FXML
    private HBox screenHBox;
    @FXML
    private VBox screenCol2;
    
    @FXML
    public void initialize() throws IOException {
        VBox sidebar = (VBox) context.loadNode(getClass().getResource("/fxml/sidebar.fxml"), new SidebarController());
        screenHBox.getChildren().add(0, sidebar);

        BorderPane header = (BorderPane) context.loadNode(getClass().getResource("/fxml/header.fxml"), new HeaderController());
        screenCol2.getChildren().add(0, header);
    }
    
}
