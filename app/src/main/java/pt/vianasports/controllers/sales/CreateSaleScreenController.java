package pt.vianasports.controllers.sales;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import pt.vianasports.controllers.GenericController;
import pt.vianasports.controllers.HeaderController;
import pt.vianasports.controllers.SidebarController;
import pt.vianasports.controllers.supplier.SupplierScreenController;
import pt.vianasports.models.Cart;
import pt.vianasports.models.CartLine;
import pt.vianasports.models.Product;
import pt.vianasports.models.ProductSize;


import java.awt.event.ActionEvent;
import java.io.IOException;

public class CreateSaleScreenController extends GenericController {

    @FXML
    private HBox screenHBox;
    @FXML
    private VBox screenCol2;

    @FXML
    private TableView salesTable;
    @FXML
    private TableColumn<CartLine, String> tableID;
    @FXML
    private TableColumn<CartLine, String> tableNome;
    @FXML
    private TableColumn<CartLine, String> tableQtd;

    @FXML
    public void initialize() throws IOException {
        context.currentPageTitle = "Criar Venda";
        Node sidebar = (Node) context.loadNode(getClass().getResource("/fxml/sidebar.fxml"), new SidebarController());
        screenHBox.getChildren().add(0, sidebar);

        Node header = (Node) context.loadNode(getClass().getResource("/fxml/header.fxml"), new HeaderController());
        screenCol2.getChildren().add(0, header);

        tableID.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getProductSize().getProduct().getId())));
        tableNome.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getProductSize().getProduct().getName())));
        tableQtd.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getQuantity())));

        // preenches a tabela com estes dados
        for (CartLine entry:
             context.cart.getEntries()) {
            salesTable.getItems().add(entry);
        }
    }


    @FXML private void cancel() throws IOException {
        this.context.currentPageTitle = "Vendas";
        CreateSaleScreenController controller = new CreateSaleScreenController();
        context.loadScreen(getClass().getResource("/fxml/supplier/supplierScreen.fxml"), controller);
    }
}
