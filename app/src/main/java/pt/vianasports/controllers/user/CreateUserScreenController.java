package pt.vianasports.controllers.user;

import java.io.File;
import java.io.FileOutputStream;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import pt.vianasports.controllers.GenericController;
import pt.vianasports.controllers.HeaderController;
import pt.vianasports.controllers.SidebarController;
import pt.vianasports.forms.Validators;
import pt.vianasports.models.Role;

import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import pt.vianasports.models.Employee;

public class CreateUserScreenController extends GenericController {


    @FXML
    private HBox screenHBox;
    @FXML
    private VBox screenCol2;

    @FXML
    private TextField nameField;
    @FXML
    private Label nameFieldError;
    @FXML
    private TextField emailField;
    @FXML
    private Label emailFieldError;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label passwordFieldError;
    @FXML
    private TextField addressField;
    @FXML
    private Label addressFieldError;
    @FXML
    private TextField phoneField;
    @FXML
    private Label phoneFieldError;
    @FXML
    private ChoiceBox roleSelect;
    @FXML
    private Label roleFieldError;
    @FXML
    private ToggleGroup genderToggleGroup;
    @FXML
    private RadioButton radioButtonFemale;
    @FXML
    private RadioButton radioButtonMale;
    @FXML
    private Label genderFieldError;
    @FXML
    private Circle photoCircle;

    private File photoFile;

    @FXML
    public void initialize() throws IOException {
        context.currentPageTitle = "Criar Utilizador";

        VBox sidebar = (VBox) context.loadNode(getClass().getResource("/fxml/sidebar.fxml"), new SidebarController());
        screenHBox.getChildren().add(0, sidebar);

        BorderPane header = (BorderPane) context.loadNode(getClass().getResource("/fxml/header.fxml"), new HeaderController());
        screenCol2.getChildren().add(0, header);

        Image photo = new Image("file:user.png");

        ImagePattern photoPattern = new ImagePattern(photo);
        photoCircle.setFill(photoPattern);
        
        final ObservableList<Role> roles = FXCollections.observableList(context.roleService.getAll());
        roleSelect.setItems(roles);

        roleSelect.setConverter(new StringConverter<Role>() {
            @Override
            public String toString(Role object) {
                return object.getName();
            }

            @Override
            public Role fromString(String string) {
               for (Role role : roles) {
                   if (role.getName().equals(string)) {
                       return role;
                   }
               }
               return null;
            }
        });

    }
    
    @FXML
    private void submit(ActionEvent event) throws IOException {
        boolean valid = true;
        nameFieldError.setManaged(false);
        nameFieldError.setVisible(false);
        nameFieldError.setText("");
        emailFieldError.setManaged(false);
        emailFieldError.setVisible(false);
        emailFieldError.setText("");
        passwordFieldError.setManaged(false);
        passwordFieldError.setVisible(false);
        passwordFieldError.setText("");
        addressFieldError.setManaged(false);
        addressFieldError.setVisible(false);
        addressFieldError.setText("");
        roleFieldError.setManaged(false);
        roleFieldError.setVisible(false);
        roleFieldError.setText("");
        genderFieldError.setManaged(false);
        genderFieldError.setVisible(false);
        genderFieldError.setText("");
        phoneFieldError.setManaged(false);
        phoneFieldError.setVisible(false);
        phoneFieldError.setText("");

        if (nameField.getText().isEmpty()) {
            nameFieldError.setText("Campo nome é obrigatório");
            nameFieldError.setManaged(true);
            nameFieldError.setVisible(true);
            valid = false;
        }

        if (passwordField.getText().isEmpty()) {
            passwordFieldError.setText("Campo password é obrigatório");
            passwordFieldError.setManaged(true);
            passwordFieldError.setVisible(true);
            valid = false;
        }

        if (emailField.getText().isEmpty()) {
            emailFieldError.setText("Campo email é obrigatório");
            emailFieldError.setManaged(true);
            emailFieldError.setVisible(true);
            valid = false;
        } else if (!Validators.isEmail(emailField.getText())) {
            emailFieldError.setText("Formato de email inválido");
            emailFieldError.setManaged(true);
            emailFieldError.setVisible(true);
            valid = false;
        }

        if (phoneField.getText().isEmpty()) {
            phoneFieldError.setText("Campo telefone é obrigatório");
            phoneFieldError.setManaged(true);
            phoneFieldError.setVisible(true);
            valid = false;
        } else if (!Validators.isPhoneNumber(phoneField.getText())) {
            phoneFieldError.setText("Formato de telefone inválido");
            phoneFieldError.setManaged(true);
            phoneFieldError.setVisible(true);
            valid = false;
        }

        if (addressField.getText().isEmpty()) {
            addressFieldError.setText("Campo morada é obrigatório");
            addressFieldError.setManaged(true);
            addressFieldError.setVisible(true);
            valid = false;
        }

            if (roleSelect.getSelectionModel().isEmpty()) {
            roleFieldError.setText("Campo cargo é obrigatório");
            roleFieldError.setManaged(true);
            roleFieldError.setVisible(true);
            valid = false;
        }

        if (!valid) {
            return;
        }

        Employee emp = new Employee();
        emp.setName(nameField.getText());
        emp.setEmail(emailField.getText());
        emp.setPassword(passwordField.getText());
        emp.setAddress(addressField.getText());
        emp.setRole((Role)roleSelect.getSelectionModel().getSelectedItem());
        emp.setPhone(phoneField.getText());
        emp.setCreator(context.loggedUser);
        emp.setGender(((RadioButton) genderToggleGroup.getSelectedToggle()).getText());

        File file = null;
        if (photoFile != null) {
            file = new File("user-photos/"+UUID.randomUUID() + photoFile.getName().substring(photoFile.getName().lastIndexOf('.')));
            file.getParentFile().mkdirs();
            file.createNewFile();
            FileOutputStream fout = new FileOutputStream(file);
            fout.write(Files.readAllBytes(photoFile.toPath()));
            fout.close();

            // TODO check file creation errors
            emp.setPhotoPath(file.getPath());
        }

        boolean success = context.empService.create(emp);
        
        if (success) {
            // TODO
            this.context.currentPageTitle = "Utilizadores";
            UserScreenController controller = new UserScreenController();
            context.loadScreen(getClass().getResource("/fxml/user/usersScreen.fxml"), controller);
        } else {
            if (file != null) {
                file.delete();
            }
        }
    }
    
    @FXML private void cancel() throws IOException {
        this.context.currentPageTitle = "Utilizadores";
        UserScreenController controller = new UserScreenController();
        context.loadScreen(getClass().getResource("/fxml/user/usersScreen.fxml"), controller);
    }
    
    // TODO crop the image to always be a square
    @FXML
    private void openFileChooser() {
        FileChooser fileChooser = new FileChooser();
          fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        photoFile = fileChooser.showOpenDialog(screenHBox.getScene().getWindow());
         if (photoFile != null) {
             Image userPhotoImg = new Image(photoFile.toURI().toString());
             photoCircle.setFill(new ImagePattern(userPhotoImg));
         }
    }
}
