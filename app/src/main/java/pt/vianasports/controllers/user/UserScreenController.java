/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.vianasports.controllers.user;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import pt.vianasports.controllers.GenericController;
import pt.vianasports.controllers.HeaderController;
import pt.vianasports.controllers.SidebarController;
import pt.vianasports.forms.Validators;
import pt.vianasports.models.Employee;
import pt.vianasports.models.Role;

/**
 *
 * @author pedro
 */
public class UserScreenController extends GenericController {

    @FXML
    private HBox screenHBox;
    @FXML
    private VBox screenCol2;

    @FXML
    private ListView usersList;
    @FXML
    private TextField searchField;

    @FXML
    private GridPane form;
    @FXML
    private TextField nameField;
    @FXML
    private Label nameFieldError;
    @FXML
    private TextField emailField;
    @FXML
    private Label emailFieldError;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label passwordFieldError;
    @FXML
    private TextField addressField;
    @FXML
    private Label addressFieldError;
    @FXML
    private TextField phoneField;
    @FXML
    private Label phoneFieldError;
    @FXML
    private ChoiceBox roleSelect;
    @FXML
    private Label roleFieldError;
    @FXML
    private ToggleGroup genderToggleGroup;
    @FXML
    private RadioButton radioButtonFemale;
    @FXML
    private RadioButton radioButtonMale;
    @FXML
    private Label genderFieldError;
    @FXML
    private Circle photoCircle;
    private File photoFile;

    private FilteredList<Employee> emps;

    
    @FXML
    public void initialize() throws IOException {
        VBox sidebar = (VBox) context.loadNode(getClass().getResource("/fxml/sidebar.fxml"), new SidebarController());
        screenHBox.getChildren().add(0, sidebar);

        BorderPane header = (BorderPane) context.loadNode(getClass().getResource("/fxml/header.fxml"), new HeaderController());
        screenCol2.getChildren().add(0, header);

        Image photo = new Image("file:user.png");

        ImagePattern photoPattern = new ImagePattern(photo);
        photoCircle.setFill(photoPattern);
        
        emps = new FilteredList<>(FXCollections.observableArrayList(context.empService.getAll()));
        
        usersList.setItems(emps);
        // TODO temp example needs to be a custom ui
        usersList.setCellFactory(param -> new ListCell<Employee>() {
            @Override
            protected void updateItem(Employee item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null || item.getName() == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    Circle circle = new Circle(25);
                    Image userPhotoImg;
                    if (item.getPhotoPath() == null) {
                        userPhotoImg = new Image("file:user.png");
                    } else {
                        userPhotoImg = new Image("file:"+item.getPhotoPath());
                    }
                    circle.setFill(new ImagePattern(userPhotoImg));
                    setGraphic(circle);
                    setText(item.getId() + "-" + item.getName());
                }
            }
        });
        
        usersList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Employee>() {
            @Override
            public void changed(ObservableValue<? extends Employee> observable, Employee oldValue, Employee newValue) {
                // TODO here we should set the input values of an edit form and make it visible
                // also hide fields or buttons depending on the logged user role
                if (newValue == null) {
                    form.setVisible(false);
                    return;
                }
                form.setVisible(true);

                nameField.setText(newValue.getName());
                emailField.setText(newValue.getEmail());
                addressField.setText(newValue.getAddress());
                phoneField.setText(newValue.getPhone());

                if (newValue.getGender().equals("Masculino")) {
                    radioButtonMale.setSelected(true);
                    radioButtonFemale.setSelected(false);
                } else if (newValue.getGender().equals("Feminino")) {
                    radioButtonFemale.setSelected(true);
                    radioButtonMale.setSelected(false);
                }

                for (Role role: (List<Role>)roleSelect.getItems()) {
                    if (role.getId() == newValue.getRole().getId()) {
                        roleSelect.getSelectionModel().select(role);
                    }
                }

                if (newValue.getPhotoPath() != null) {
                    Image userPhotoImg = new Image("file:"+newValue.getPhotoPath());
                    photoCircle.setFill(new ImagePattern(userPhotoImg));
                } else {
                    Image userPhotoImg = new Image("file:user.png");
                    photoCircle.setFill(new ImagePattern(userPhotoImg));
                }
            }
        });
        
        searchField.textProperty().addListener((observable, oldValue, newValue) -> {
            emps.setPredicate(emp -> emp.getName().toLowerCase().contains(newValue.toLowerCase()) || 
                    String.valueOf(emp.getId()).contains(newValue)
            );
        });
        
        final ObservableList<Role> roles = FXCollections.observableList(context.roleService.getAll());
        roleSelect.setItems(roles);
        roleSelect.setConverter(new StringConverter<Role>() {
            @Override
            public String toString(Role object) {
                return object.getName();
            }

            @Override
            public Role fromString(String string) {
                for (Role role : roles) {
                    if (role.getName().equals(string)) {
                        return role;
                    }
                }
                return null;
            }
        });
    }

    @FXML
    private void loadCreateUser(ActionEvent event) throws IOException {
        CreateUserScreenController controller = new CreateUserScreenController();
        context.loadScreen(getClass().getResource("/fxml/user/userForm.fxml"), controller);
    }

    @FXML
    private void update(ActionEvent event) throws IOException {
        boolean valid = true;
        nameFieldError.setManaged(false);
        nameFieldError.setVisible(false);
        nameFieldError.setText("");
        emailFieldError.setManaged(false);
        emailFieldError.setVisible(false);
        emailFieldError.setText("");
        passwordFieldError.setManaged(false);
        passwordFieldError.setVisible(false);
        passwordFieldError.setText("");
        addressFieldError.setManaged(false);
        addressFieldError.setVisible(false);
        addressFieldError.setText("");
        roleFieldError.setManaged(false);
        roleFieldError.setVisible(false);
        roleFieldError.setText("");
        genderFieldError.setManaged(false);
        genderFieldError.setVisible(false);
        genderFieldError.setText("");
        phoneFieldError.setManaged(false);
        phoneFieldError.setVisible(false);
        phoneFieldError.setText("");

        if (nameField.getText().isEmpty()) {
            nameFieldError.setText("Campo nome é obrigatório");
            nameFieldError.setManaged(true);
            nameFieldError.setVisible(true);
            valid = false;
        }

        if (emailField.getText().isEmpty()) {
            emailFieldError.setText("Campo email é obrigatório");
            emailFieldError.setManaged(true);
            emailFieldError.setVisible(true);
            valid = false;
        } else if (!Validators.isEmail(emailField.getText())) {
            emailFieldError.setText("Formato de email inválido");
            emailFieldError.setManaged(true);
            emailFieldError.setVisible(true);
             valid = false;
        }

        if (phoneField.getText().isEmpty()) {
            phoneFieldError.setText("Campo telefone é obrigatório");
            phoneFieldError.setManaged(true);
            phoneFieldError.setVisible(true);
            valid = false;
        } else if (!Validators.isPhoneNumber(phoneField.getText())) {
                phoneFieldError.setText("Formato de telefone inválido");
                phoneFieldError.setManaged(true);
                phoneFieldError.setVisible(true);
                valid = false;
        }

        if (addressField.getText().isEmpty()) {
            addressFieldError.setText("Campo morada é obrigatório");
            addressFieldError.setManaged(true);
            addressFieldError.setVisible(true);
            valid = false;
        }

        // TODO role and gender validation
        if (!valid) {
            return;
        }

        Employee emp = (Employee) usersList.getSelectionModel().getSelectedItem();
        emp.setName(nameField.getText());
        emp.setEmail(emailField.getText());
        if (!passwordField.getText().isEmpty()) {
            emp.setPassword(passwordField.getText());
        }
        emp.setAddress(addressField.getText());
        emp.setRole((Role)roleSelect.getSelectionModel().getSelectedItem());
        emp.setPhone(phoneField.getText());
        emp.setCreator(context.loggedUser);
        emp.setGender(((RadioButton) genderToggleGroup.getSelectedToggle()).getText());

        File file = null;
        if (photoFile != null) {
            emp.setPhotoPath(photoFile.getPath());
            file = new File("user-photos/"+UUID.randomUUID() + photoFile.getName().substring(photoFile.getName().lastIndexOf('.')));
            file.getParentFile().mkdirs();
            file.createNewFile();
            FileOutputStream fout = new FileOutputStream(file);
            fout.write(Files.readAllBytes(photoFile.toPath()));
            fout.close();
            // TODO check file creation errors
            emp.setPhotoPath(file.getPath());
        }

        boolean success = context.empService.update(emp, context.loggedUser);

        if (success) {
            ObservableList<Employee> source = (ObservableList<Employee>) emps.getSource();
            for (int i = 0; i < source.size(); i++) {
                if(source.get(i).getId() == emp.getId()) {
                    source.set(i, emp);
                }
            }
        } else {
            if (file != null) {
                file.delete();
            }
        }
    }

    @FXML
    private void delete(ActionEvent event) throws IOException {
        Employee emp = (Employee) usersList.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Remover Utilizador");
        alert.setHeaderText("Comfirmar remoção de utilizador");
        alert.setContentText(emp.getId() + "-" + emp.getName());

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            if(context.empService.delete(emp)) {
                emps.getSource().remove(emp);
                if (emp.getPhotoPath() != null) {
                    File file = new File(emp.getPhotoPath());
                    file.delete();
                }
            }
        }
    }
    
        // TODO crop the image to always be a square
    @FXML
    private void openFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        photoFile = fileChooser.showOpenDialog(screenHBox.getScene().getWindow());
        if (photoFile != null) {
            Image userPhotoImg = new Image(photoFile.toURI().toString());
            photoCircle.setFill(new ImagePattern(userPhotoImg));
        }
    }
    
}
