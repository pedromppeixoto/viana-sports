package pt.vianasports.controllers.supplier;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import pt.vianasports.controllers.GenericController;
import pt.vianasports.controllers.HeaderController;
import pt.vianasports.controllers.SidebarController;
import pt.vianasports.forms.Validators;
import pt.vianasports.models.Supplier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

public class CreateSupplierController extends GenericController {

    @FXML
    private HBox screenHBox;
    @FXML
    private VBox screenCol2;

    @FXML
    private TextField nameField;
    @FXML
    private Label nameFieldError;
    @FXML
    private TextField emailField;
    @FXML
    private Label emailFieldError;
    @FXML
    private TextField nifField;
    @FXML
    private Label nifFieldError;
    @FXML
    private TextField addressField;
    @FXML
    private Label addressFieldError;
    @FXML
    private TextField phoneField;
    @FXML
    private Label phoneFieldError;
    @FXML
    private Circle photoCircle;

    private File photoFile;

    @FXML
    public void initialize() throws IOException {
        context.currentPageTitle = "Criar Fornecedor";
        VBox sidebar = (VBox) context.loadNode(getClass().getResource("/fxml/sidebar.fxml"), new SidebarController());
        screenHBox.getChildren().add(0, sidebar);

        BorderPane header = (BorderPane) context.loadNode(getClass().getResource("/fxml/header.fxml"), new HeaderController());
        screenCol2.getChildren().add(0, header);

        Image photo = new Image("file:user.png");

        ImagePattern photoPattern = new ImagePattern(photo);
        photoCircle.setFill(photoPattern);
    }

    @FXML
    private void submit(ActionEvent event) throws IOException {
        boolean valid = true;
        nameFieldError.setManaged(false);
        nameFieldError.setVisible(false);
        nameFieldError.setText("");
        nifFieldError.setManaged(false);
        nifFieldError.setVisible(false);
        nifFieldError.setText("");
        emailFieldError.setManaged(false);
        emailFieldError.setVisible(false);
        emailFieldError.setText("");
        addressFieldError.setManaged(false);
        addressFieldError.setVisible(false);
        addressFieldError.setText("");
        phoneFieldError.setManaged(false);
        phoneFieldError.setVisible(false);
        phoneFieldError.setText("");

        if (nameField.getText().isEmpty()) {
            nameFieldError.setText("Campo nome é obrigatório");
            nameFieldError.setManaged(true);
            nameFieldError.setVisible(true);
            valid = false;
        }
        if (nifField.getText().isEmpty()) {
            nifFieldError.setText("Campo nif é obrigatório");
            nifFieldError.setManaged(true);
            nifFieldError.setVisible(true);
            valid = false;
        } else if (!Validators.isNif(nifField.getText())) {
            nifFieldError.setText("Formato de NIF inválido");
            nifFieldError.setManaged(true);
            nifFieldError.setVisible(true);
        }

        if (emailField.getText().isEmpty()) {
            emailFieldError.setText("Campo email é obrigatório");
            emailFieldError.setManaged(true);
            emailFieldError.setVisible(true);
            valid = false;
        } else if (!Validators.isEmail(emailField.getText())) {
            emailFieldError.setText("Formato de email inválido");
            emailFieldError.setManaged(true);
            emailFieldError.setVisible(true);
            valid = false;
        }

        if (addressField.getText().isEmpty()) {
            addressFieldError.setText("Campo morada é obrigatório");
            addressFieldError.setManaged(true);
            addressFieldError.setVisible(true);
            valid = false;
        }

        if (phoneField.getText().isEmpty()) {
            phoneFieldError.setText("Campo telefone é obrigatório");
            phoneFieldError.setManaged(true);
            phoneFieldError.setVisible(true);
            valid = false;
        } else if (!Validators.isPhoneNumber(phoneField.getText())) {
            phoneFieldError.setText("Formato de telefone inválido");
            phoneFieldError.setManaged(true);
            phoneFieldError.setVisible(true);
            valid = false;
        }

        if (!valid) {
            return;
        }

        Supplier supplier = new Supplier();
        supplier.setName(nameField.getText());
        supplier.setEmail(emailField.getText());
        supplier.setAddress(addressField.getText());
        supplier.setNif(Integer.valueOf(nifField.getText()));
        supplier.setPhone(phoneField.getText());
        supplier.setCreator(context.loggedUser);

        File file = null;
        if (photoFile != null) {
            file = new File("supplier-photos/"+UUID.randomUUID() + photoFile.getName().substring(photoFile.getName().lastIndexOf('.')));
            file.getParentFile().mkdirs();
            file.createNewFile();
            FileOutputStream fout = new FileOutputStream(file);
            fout.write(Files.readAllBytes(photoFile.toPath()));
            fout.close();

            // TODO check file creation errors
            supplier.setPhotoPath(file.getPath());
        }

        boolean success = context.supplierService.create(supplier);

        if (success) {
            this.context.currentPageTitle = "Suppliers";
            SupplierScreenController controller = new SupplierScreenController();
            context.loadScreen(getClass().getResource("/fxml/supplier/supplierScreen.fxml"), controller);
        } else {
            if (file!=null) {
                file.delete();
            }
        }
    }

    @FXML private void cancel() throws IOException {
        this.context.currentPageTitle = "Suppliers";
        SupplierScreenController controller = new SupplierScreenController();
        context.loadScreen(getClass().getResource("/fxml/supplier/supplierScreen.fxml"), controller);
    }

    // TODO crop the image to always be a square
    @FXML
    private void openFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        photoFile = fileChooser.showOpenDialog(screenHBox.getScene().getWindow());
        if (photoFile != null) {
            Image photo = new Image(photoFile.toURI().toString());
            photoCircle.setFill(new ImagePattern(photo));
        }
    }
}
