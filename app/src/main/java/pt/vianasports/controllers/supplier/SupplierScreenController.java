package pt.vianasports.controllers.supplier;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import pt.vianasports.controllers.GenericController;
import pt.vianasports.controllers.HeaderController;
import pt.vianasports.controllers.SidebarController;
import pt.vianasports.forms.Validators;
import pt.vianasports.models.Supplier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;
import java.util.UUID;

public class SupplierScreenController extends GenericController {

    @FXML
    private HBox screenHBox;
    @FXML
    private VBox screenCol2;

    @FXML
    private ListView supplierList;
    @FXML
    private TextField searchField;

    @FXML
    private TextField nameField;
    @FXML
    private Label nameFieldError;
    @FXML
    private TextField emailField;
    @FXML
    private Label emailFieldError;
    @FXML
    private TextField nifField;
    @FXML
    private Label nifFieldError;
    @FXML
    private TextField addressField;
    @FXML
    private Label addressFieldError;
    @FXML
    private TextField phoneField;
    @FXML
    private Label phoneFieldError;
    @FXML
    private Circle photoCircle;

    private File photoFile;
    private FilteredList<Supplier> suppliers;

    @FXML
    public void initialize() throws IOException {
        VBox sidebar = (VBox) context.loadNode(getClass().getResource("/fxml/sidebar.fxml"), new SidebarController());
        screenHBox.getChildren().add(0, sidebar);

        BorderPane header = (BorderPane) context.loadNode(getClass().getResource("/fxml/header.fxml"), new HeaderController());
        screenCol2.getChildren().add(0, header);

        Image photo = new Image("file:user.png");

        ImagePattern photoPattern = new ImagePattern(photo);
        photoCircle.setFill(photoPattern);

        suppliers = new FilteredList<>(FXCollections.observableArrayList(context.supplierService.getAll()));

        supplierList.setItems(suppliers);

        supplierList.setCellFactory(param -> new ListCell<Supplier>() {
            @Override
            protected void updateItem(Supplier item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null || item.getName() == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    Circle circle = new Circle(25);
                    Image userPhotoImg;
                    if (item.getPhotoPath() == null) {
                        userPhotoImg = new Image("file:user.png");
                    } else {
                        userPhotoImg = new Image("file:"+item.getPhotoPath());
                    }
                    circle.setFill(new ImagePattern(userPhotoImg));
                    setGraphic(circle);
                    setText(item.getId() + "-" + item.getName());
                }
            }
        });

        supplierList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Supplier>() {
            @Override
            public void changed(ObservableValue<? extends Supplier> observable, Supplier oldValue, Supplier newValue) {
                // TODO here we should set the input values of an edit form and make it visible
                // also hide fields or buttons depending on the logged user role
                nameField.setText(newValue.getName());
                emailField.setText(newValue.getEmail());
                addressField.setText(newValue.getAddress());
                phoneField.setText(newValue.getPhone());
                nifField.setText(String.valueOf(newValue.getNif()));

                if (newValue.getPhotoPath() != null) {
                    Image userPhotoImg = new Image("file:"+newValue.getPhotoPath());
                    photoCircle.setFill(new ImagePattern(userPhotoImg));
                } else {
                    Image userPhotoImg = new Image("file:user.png");
                    photoCircle.setFill(new ImagePattern(userPhotoImg));
                }
            }
        });
    }

    @FXML
    private void loadCreateSupplier(ActionEvent event) throws IOException {
        CreateSupplierController controller = new CreateSupplierController();
        context.loadScreen(getClass().getResource("/fxml/supplier/supplierForm.fxml"), controller);
    }

    @FXML
    private void openFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        photoFile = fileChooser.showOpenDialog(screenHBox.getScene().getWindow());
        if (photoFile != null) {
            Image photo = new Image(photoFile.toURI().toString());
            photoCircle.setFill(new ImagePattern(photo));
        }
    }

    @FXML
    private void submit(ActionEvent event) throws IOException {
        boolean valid = true;
        nameFieldError.setManaged(false);
        nameFieldError.setVisible(false);
        nameFieldError.setText("");
        nifFieldError.setManaged(false);
        nifFieldError.setVisible(false);
        nifFieldError.setText("");
        emailFieldError.setManaged(false);
        emailFieldError.setVisible(false);
        emailFieldError.setText("");
        addressFieldError.setManaged(false);
        addressFieldError.setVisible(false);
        addressFieldError.setText("");
        phoneFieldError.setManaged(false);
        phoneFieldError.setVisible(false);
        phoneFieldError.setText("");

        if (nameField.getText().isEmpty()) {
            nameFieldError.setText("Campo nome é obrigatório");
            nameFieldError.setManaged(true);
            nameFieldError.setVisible(true);
            valid = false;
        }
        if (nifField.getText().isEmpty()) {
            nifFieldError.setText("Campo nif é obrigatório");
            nifFieldError.setManaged(true);
            nifFieldError.setVisible(true);
            valid = false;
        } else if (!Validators.isNif(nifField.getText())) {
            nifFieldError.setText("Formato de NIF inválido");
            nifFieldError.setManaged(true);
            nifFieldError.setVisible(true);
        }

        if (emailField.getText().isEmpty()) {
            emailFieldError.setText("Campo email é obrigatório");
            emailFieldError.setManaged(true);
            emailFieldError.setVisible(true);
            valid = false;
        } else if (!Validators.isEmail(emailField.getText())) {
            emailFieldError.setText("Formato de email inválido");
            emailFieldError.setManaged(true);
            emailFieldError.setVisible(true);
            valid = false;
        }

        if (addressField.getText().isEmpty()) {
            addressFieldError.setText("Campo morada é obrigatório");
            addressFieldError.setManaged(true);
            addressFieldError.setVisible(true);
            valid = false;
        }

        if (phoneField.getText().isEmpty()) {
            phoneFieldError.setText("Campo telefone é obrigatório");
            phoneFieldError.setManaged(true);
            phoneFieldError.setVisible(true);
            valid = false;
        } else if (!Validators.isPhoneNumber(phoneField.getText())) {
            phoneFieldError.setText("Formato de telefone inválido");
            phoneFieldError.setManaged(true);
            phoneFieldError.setVisible(true);
            valid = false;
        }

        if (!valid) {
            return;
        }

        Supplier supplier = (Supplier) supplierList.getSelectionModel().getSelectedItem();
        supplier.setName(nameField.getText());
        supplier.setEmail(emailField.getText());
        supplier.setAddress(addressField.getText());
        supplier.setNif(Integer.valueOf(nifField.getText()));
        supplier.setPhone(phoneField.getText());
        supplier.setCreator(context.loggedUser);

        File file = null;
        if (photoFile != null) {
            file = new File("supplier-photos/"+UUID.randomUUID() + photoFile.getName().substring(photoFile.getName().lastIndexOf('.')));
            file.getParentFile().mkdirs();
            file.createNewFile();
            FileOutputStream fout = new FileOutputStream(file);
            fout.write(Files.readAllBytes(photoFile.toPath()));
            fout.close();

            // TODO check file creation errors
            supplier.setPhotoPath(file.getPath());
        }


        boolean success = context.supplierService.update(supplier, context.loggedUser);

        if (success) {
            ObservableList<Supplier> source = (ObservableList<Supplier>) suppliers.getSource();
            for (int i = 0; i < source.size(); i++) {
                if(source.get(i).getId() == supplier.getId()) {
                    source.set(i, supplier);
                }
            }
        } else {
            if (file != null) {
                file.delete();
            }
        }
    }

    @FXML
    private void delete(ActionEvent event) throws IOException {
        Supplier supplier = (Supplier) supplierList.getSelectionModel().getSelectedItem();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Remover Fornecedor");
        alert.setHeaderText("Comfirmar remoção de fornecedor");
        alert.setContentText(supplier.getId() + "-" + supplier.getName());

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            if(context.supplierService.delete(supplier)) {
                suppliers.getSource().remove(supplier);
                if (supplier.getPhotoPath() != null) {
                    File file = new File(supplier.getPhotoPath());
                    file.delete();
                }
            }
        }
    }
}
