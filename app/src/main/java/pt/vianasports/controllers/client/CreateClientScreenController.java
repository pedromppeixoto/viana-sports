package pt.vianasports.controllers.client;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import pt.vianasports.controllers.GenericController;
import pt.vianasports.controllers.HeaderController;
import pt.vianasports.controllers.SidebarController;
import pt.vianasports.forms.Validators;
import pt.vianasports.models.Client;

public class CreateClientScreenController extends GenericController {

    @FXML
    private HBox screenHBox;
    @FXML
    private VBox screenCol2;

    @FXML
    private TextField nameField;
    @FXML
    private Label nameFieldError;
    @FXML
    private TextField emailField;
    @FXML
    private Label emailFieldError;
    @FXML
    private TextField nifField;
    @FXML
    private Label nifFieldError;
    @FXML
    private TextField addressField;
    @FXML
    private Label addressFieldError;
    @FXML
    private TextField phoneField;
    @FXML
    private Label phoneFieldError;
    @FXML
    private ToggleGroup genderToggleGroup;
    @FXML
    private Label gender;
    @FXML
    private Label genderFieldError;
    @FXML
    private Circle photoCircle;
    private File photoFile;

    @FXML
    public void initialize() throws IOException {
        context.currentPageTitle = "Criar Cliente";
        Node sidebar = (Node) context.loadNode(getClass().getResource("/fxml/sidebar.fxml"), new SidebarController());
        screenHBox.getChildren().add(0, sidebar);

        Node header = (Node) context.loadNode(getClass().getResource("/fxml/header.fxml"), new HeaderController());
        screenCol2.getChildren().add(0, header);

        Image photo = new Image("file:user.png");

        ImagePattern photoPattern = new ImagePattern(photo);
        photoCircle.setFill(photoPattern);
    }
    @FXML
    private void submit(ActionEvent event) throws IOException {
        boolean valid = true;

        nameFieldError.setManaged(false);
        nameFieldError.setVisible(false);
        nameFieldError.setText("");
        nifFieldError.setManaged(false);
        nifFieldError.setVisible(false);
        nifFieldError.setText("");
        emailFieldError.setManaged(false);
        emailFieldError.setVisible(false);
        emailFieldError.setText("");
        addressFieldError.setManaged(false);
        addressFieldError.setVisible(false);
        addressFieldError.setText("");
        phoneFieldError.setManaged(false);
        phoneFieldError.setVisible(false);
        phoneFieldError.setText("");
        genderFieldError.setManaged(false);
        genderFieldError.setVisible(false);
        genderFieldError.setText("");

        if (nameField.getText().isEmpty()) {
            nameFieldError.setText("Campo nome é obrigatório");
            nameFieldError.setManaged(true);
            nameFieldError.setVisible(true);
            valid = false;
        }
        if (nifField.getText().isEmpty()) {
            nifFieldError.setText("Campo nif é obrigatório");
            nifFieldError.setManaged(true);
            nifFieldError.setVisible(true);
            valid = false;
        } else if (!Validators.isNif(nifField.getText())) {
            nifFieldError.setText("Formato de NIF inválido");
            nifFieldError.setManaged(true);
            nifFieldError.setVisible(true);
        }

        if (!emailField.getText().isEmpty()) {
            if (!Validators.isEmail(emailField.getText())) {
                emailFieldError.setText("Formato de email inválido");
                emailFieldError.setManaged(true);
                emailFieldError.setVisible(true);
                valid = false;
            }
        }
        if (!phoneField.getText().isEmpty()) {
            if (!Validators.isPhoneNumber(phoneField.getText())) {
                phoneFieldError.setText("Formato de telefone inválido");
                phoneFieldError.setManaged(true);
                phoneFieldError.setVisible(true);
                valid = false;
            }
        }

        if (!valid) {
            return;
        }

        Client client = new Client();
        client.setName(nameField.getText());
        client.setEmail(emailField.getText());
        client.setAddress(addressField.getText());
        client.setNif(Integer.valueOf(nifField.getText()));
        client.setPhone(phoneField.getText());
        client.setCreator(context.loggedUser);
        client.setGender(((RadioButton) genderToggleGroup.getSelectedToggle()).getText());

        File file = null;
        if (photoFile!=null) {
            file = new File("client-photos/"+UUID.randomUUID() + photoFile.getName().substring(photoFile.getName().lastIndexOf('.')));
            file.getParentFile().mkdirs();
            file.createNewFile();
            FileOutputStream fout = new FileOutputStream(file);
            fout.write(Files.readAllBytes(photoFile.toPath()));
            fout.close();

            // TODO check file creation errors
            client.setPhotoPath(file.getPath());
        }

        boolean success = context.clientService.create(client);

        if (success) {
            this.context.currentPageTitle = "Clientes";
            ClientScreenController controller = new ClientScreenController();
            context.loadScreen(getClass().getResource("/fxml/clients/clientScreen.fxml"), controller);
        } else {
            if (file != null) {
                file.delete();
            }
        }
    }

    @FXML private void cancel() throws IOException {
        this.context.currentPageTitle = "Clientes";
        ClientScreenController controller = new ClientScreenController();
        context.loadScreen(getClass().getResource("/fxml/clients/clientScreen.fxml"), controller);
    }

    @FXML
    private void openFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        photoFile = fileChooser.showOpenDialog(screenHBox.getScene().getWindow());
        if (photoFile != null) {
            Image photo = new Image(photoFile.toURI().toString());
            photoCircle.setFill(new ImagePattern(photo));
        }
    }
    
    
}
