/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.vianasports.controllers;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import pt.vianasports.controllers.login.LoginController;

/**
 *
 * @author pedro
 */
public class HeaderController extends GenericController {
   
    @FXML
    private Button logoutBtn;
    @FXML
    private Label userNameLabel;
    @FXML
    private Label pageTitleLable;
 
    @FXML
    public void initialize() {
        this.userNameLabel.setText(this.context.loggedUser.getName());
        this.pageTitleLable.setText(this.context.currentPageTitle);
    }

    @FXML
    private void logout(ActionEvent event) throws IOException {
        this.context.loggedUser = null;
        context.loadScreen(getClass().getResource("/fxml/login.fxml"), new LoginController());
    }
    
    
}
