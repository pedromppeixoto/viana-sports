package pt.vianasports.controllers.product;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import pt.vianasports.controllers.GenericController;
import pt.vianasports.controllers.HeaderController;
import pt.vianasports.controllers.SidebarController;
import pt.vianasports.models.Category;
import pt.vianasports.models.Product;
import pt.vianasports.models.ProductSize;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class ProductsScreenController extends GenericController {
    @FXML
    private HBox screenHBox;
    @FXML
    private VBox screenCol2;

    @FXML
    private ComboBox categoryField;
    @FXML
    private TextField searchField;

    @FXML
    private FlowPane productsPane;

    private List<Product> products;

    @FXML
    public void initialize() throws IOException {
        VBox sidebar = (VBox) context.loadNode(getClass().getResource("/fxml/sidebar.fxml"), new SidebarController());
        screenHBox.getChildren().add(0, sidebar);

        BorderPane header = (BorderPane) context.loadNode(getClass().getResource("/fxml/header.fxml"), new HeaderController());
        screenCol2.getChildren().add(0, header);

        final ObservableList<Category> categories = FXCollections.observableList(context.categoryService.getAll());
        categoryField.setItems(categories);

        categoryField.setConverter(new StringConverter<Category>() {
            @Override
            public String toString(Category object) {
                if (object != null) {
                    return object.getName();
                }
                return "";
            }

            @Override
            public Category fromString(String string) {
                for (Category category : categories) {
                    if (category.getName().equals(string)) {
                        return category;
                    }
                }
                return null;
            }
        });

        products = context.productService.getAll();

        populateProducts(products);

        searchField.textProperty().addListener((observable, oldValue, newValue) -> {
            List<Product> filteredProducts = products.stream().filter(product -> product.getName().contains(newValue))
                    .collect(Collectors.toList());
            populateProducts(filteredProducts);
        });

        categoryField.getSelectionModel().selectedItemProperty().addListener((ChangeListener<Category>) (observableValue, oldCategory, newCategory) -> {
            List<Product> filteredProducts = products.stream().filter(product -> product.getCategories().stream()
                    .anyMatch(category -> category.getId() == newCategory.getId()))
                    .collect(Collectors.toList());
            populateProducts(filteredProducts);

        });
    }

    public void removeProduct(Product product) {
        products.remove(product);
        populateProducts(products);
    }

    private void populateProducts(List<Product> products) {
        productsPane.getChildren().clear();

        for (Product product:
                products) {
            VBox productCell = null;
            try {
                productCell = (VBox)context.loadNode(getClass().getResource("/fxml/product/productCell.fxml"), new ProductCellController(product, this));
                productsPane.getChildren().add(productCell);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void loadCreateProduct(ActionEvent event) throws IOException {
        CreateProductScreenController controller = new CreateProductScreenController(false, 0);
        context.loadScreen(getClass().getResource("/fxml/product/productForm.fxml"), controller);
    }

}
