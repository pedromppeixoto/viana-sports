package pt.vianasports.controllers.product;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import pt.vianasports.controllers.GenericController;
import pt.vianasports.controllers.HeaderController;
import pt.vianasports.controllers.SidebarController;
import pt.vianasports.forms.Formatters;
import pt.vianasports.models.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.*;

public class CreateProductScreenController extends GenericController {
    private final long productId;
    private boolean updating;
    private List<Category> newCategoryList;
    private List<Category> existingCategoryList;
    private List<ProductSize> sizes;

    @FXML
    private HBox screenHBox;
    @FXML
    private VBox screenCol2;

    @FXML
    private TextField nameField;
    @FXML
    private TextField unitPriceField;
    @FXML
    private TextField stockField;
    @FXML
    private TextField vatField;
    @FXML
    private TextField unitField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField brandField;
    @FXML
    private HTMLEditor descField;
    @FXML
    private ComboBox categoryField;
    @FXML
    private Label categoryListLabel;
    @FXML
    private ImageView photoView;

    @FXML
    private TableView sizesTable;
    @FXML
    private TableColumn tableSize;
    @FXML
    private TableColumn tableUnitPrice;
    @FXML
    private TableColumn tableStock;
    @FXML
    private Label totalStockLabel;

    private File photoFile;

    public CreateProductScreenController(boolean updating, long productId) {
        this.updating = updating;
        this.productId = productId;
    }

    @FXML
    public void initialize() throws IOException {
        VBox sidebar = (VBox) context.loadNode(getClass().getResource("/fxml/sidebar.fxml"), new SidebarController());
        screenHBox.getChildren().add(0, sidebar);

        BorderPane header = (BorderPane) context.loadNode(getClass().getResource("/fxml/header.fxml"), new HeaderController());
        screenCol2.getChildren().add(0, header);

        unitPriceField.setTextFormatter(Formatters.decimalFormatter());
        stockField.setTextFormatter(Formatters.intFormatter());
        vatField.setTextFormatter(Formatters.decimalFormatter());

        newCategoryList = new ArrayList<>();
        existingCategoryList = new ArrayList<>();
        sizes = new ArrayList<>();

        final ObservableList<Category> categories = FXCollections.observableList(context.categoryService.getAll());
        categoryField.setItems(categories);
        categoryField.setEditable(true);

        categoryField.setConverter(new StringConverter<Category>() {
            @Override
            public String toString(Category object) {
                if (object != null) {
                    return object.getName();
                }
                return "";
            }

            @Override
            public Category fromString(String string) {
                for (Category category : categories) {
                    if (category.getName().equals(string)) {
                        return category;
                    }
                }
                return null;
            }
        });

        tableSize.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableStock.setCellValueFactory(new PropertyValueFactory<>("stock"));
        tableUnitPrice.setCellValueFactory(new PropertyValueFactory<>("unitPrice"));
        totalStockLabel.setText("Stock total: 0");

        photoView.setImage(new Image("file:user.png"));

        if (this.updating) {
            Product product = context.productService.getById(productId);
            //TODO populate
        }
    }

    @FXML
    private void openFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        photoFile = fileChooser.showOpenDialog(screenHBox.getScene().getWindow());
        if (photoFile != null) {
            Image photoImg = new Image(photoFile.toURI().toString());
            photoView.setImage(photoImg);
        }
    }

    @FXML
    private void deleteSizeRow(ActionEvent event) {
        ProductSize selectedItem = (ProductSize) sizesTable.getSelectionModel().getSelectedItem();
        sizesTable.getItems().remove(selectedItem);
        sizes.remove(selectedItem);
        int stockSum = 0;
        for (ProductSize item :
                sizes) {
            stockSum = stockSum + item.getStock();
        }
        totalStockLabel.setText("Stock total: " + stockSum);
    }

    @FXML
    private void addSize(ActionEvent event) {
        ProductSize size = new ProductSize();
        size.setName(sizeField.getText());
        size.setStock(Integer.parseInt(stockField.getText()));
        size.setUnitPrice(new BigDecimal(unitPriceField.getText()));
        sizeField.clear();
        stockField.clear();
        unitPriceField.clear();
        sizesTable.getItems().add(size);
        sizes.add(size);

        int stockSum = 0;
        for (ProductSize item :
                sizes) {
            stockSum = stockSum + item.getStock();
        }
        totalStockLabel.setText("Stock total: " + stockSum);
    }

    @FXML
    private void addCategory(ActionEvent event) {
        Category category = new Category();
        String categoryListText = categoryListLabel.getText();
        boolean alreadyAdded = false;

        if (categoryField.getSelectionModel().isEmpty() && !categoryField.getEditor().getText().isEmpty()) {
            category.setName(categoryField.getEditor().getText());
            for (Category item:
                    newCategoryList) {
                if (item.getName().equals(category.getName())) {
                    alreadyAdded = true;
                }
            }
            if (!alreadyAdded) {
                newCategoryList.add(category);
            }
        } else {
            category = (Category)categoryField.getSelectionModel().getSelectedItem();
            for (Category item:
                 existingCategoryList) {
                if (item.getId() == category.getId()) {
                    alreadyAdded = true;
                }
            }
            if (!alreadyAdded) {
                existingCategoryList.add(category);
            }
        }

        categoryField.getEditor().setText("");
        categoryField.getSelectionModel().clearSelection();

        if (alreadyAdded) {
            return;
        }

        if (categoryListText.isEmpty()) {
            categoryListLabel.setText(category.getName());
        } else {
            categoryListLabel.setText(categoryListText + ", " + category.getName());
        }
    }

    @FXML
    private void submit(ActionEvent event) throws IOException {
        Product product = new Product();
        product.setName(nameField.getText());
        product.setBrand(brandField.getText());
        product.setVatPercent(Integer.parseInt(vatField.getText()));
        product.setDescription(descField.getHtmlText());
        Set<Category> categories = new HashSet<Category>();
        categories.addAll(newCategoryList);
        categories.addAll(existingCategoryList);
        product.setCategories(categories);
        product.setSizes(new HashSet(sizes));

        File file = null;
        if (photoFile!=null) {
            file = new File("product-photos/"+ UUID.randomUUID() + photoFile.getName().substring(photoFile.getName().lastIndexOf('.')));
            file.getParentFile().mkdirs();
            file.createNewFile();
            FileOutputStream fout = new FileOutputStream(file);
            fout.write(Files.readAllBytes(photoFile.toPath()));
            fout.close();

            // TODO check file creation errors
            product.setPhotoPath(file.getPath());
        }

        if (updating) {
            // TODO update photo;
            context.productService.update(product, context.loggedUser);
        } else {
            boolean success = context.productService.create(product, context.loggedUser);

            if (success) {
                this.context.currentPageTitle = "Produtos";
                ProductsScreenController controller = new ProductsScreenController();
                context.loadScreen(getClass().getResource("/fxml/product/productsScreen.fxml"), controller);
            } else {
                if (file != null) {
                    file.delete();
                }
            }
        }
    }

    @FXML
    private void cancel(ActionEvent event) throws IOException {
        this.context.currentPageTitle = "Products";
        ProductsScreenController controller = new ProductsScreenController();
        context.loadScreen(getClass().getResource("/fxml/product/productsScreen.fxml"), controller);
    }
}
