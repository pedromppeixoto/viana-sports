package pt.vianasports.controllers.product;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.StringConverter;
import pt.vianasports.controllers.GenericController;
import pt.vianasports.forms.Formatters;
import pt.vianasports.models.Product;
import pt.vianasports.models.ProductSize;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductCellController extends GenericController {

    private final ProductsScreenController parentController;
    Product product;
    @FXML
    private Label nameLabel;
    @FXML
    private Label priceLabel;
    @FXML
    private Label stockLabel;
    @FXML
    private ChoiceBox sizeField;
    @FXML
    private ImageView photoView;
    @FXML
    private TextField quantityField;


    public ProductCellController(Product product, ProductsScreenController parentController) {
        this.product = product;
        this.parentController = parentController;
    }

    @FXML
    public void initialize() {
        nameLabel.setText(product.getName());
        List<ProductSize> sizes = new ArrayList<>();
        sizes.addAll(product.getSizes());
        sizeField.setItems(FXCollections.observableList(sizes));

        sizeField.setConverter(new StringConverter<ProductSize>() {
            @Override
            public String toString(ProductSize object) {
                return object.getName();
            }

            @Override
            public ProductSize fromString(String string) {
                for (ProductSize size : sizes) {
                    if (size.getName().equals(string)) {
                        return size;
                    }
                }
                return null;
            }
        });

        sizeField.getSelectionModel().selectedItemProperty().addListener((ChangeListener<ProductSize>) (observableValue, oldSize, newSize) -> {
            stockLabel.setText("stock: " + newSize.getStock().toString());
            priceLabel.setText("preço: " + newSize.getUnitPrice().toString());
        });

        sizeField.getSelectionModel().selectFirst();

        if (product.getPhotoPath() != null) {
            photoView.setImage(new Image("file:"+product.getPhotoPath()));
        }

        quantityField.setTextFormatter(Formatters.intFormatter());
        quantityField.setText("1");
    }

    @FXML
    private void update(ActionEvent event) throws IOException {
        CreateProductScreenController controller = new CreateProductScreenController(true, product.getId());
        context.loadScreen(getClass().getResource("/fxml/product/productForm.fxml"), controller);
    }

    @FXML
    private void addToCart(ActionEvent event) {
        ProductSize productSize = (ProductSize) sizeField.getSelectionModel().getSelectedItem();
        context.cart.addToCart(productSize, Integer.parseInt(quantityField.getText()));
    }

    @FXML
    private void delete(ActionEvent event) throws IOException {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Remover Produto");
        alert.setHeaderText("Comfirmar remoção de produto");
        alert.setContentText(product.getId() + "-" + product.getName());

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            if(context.productService.delete(product)) {
                parentController.removeProduct(product);
            }
        }
    }
}
