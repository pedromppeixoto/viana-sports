package pt.vianasports;

import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;
import pt.vianasports.controllers.login.LoginController;
import pt.vianasports.utils.HibernateUtil;


public class App extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        AppContext appContext = new AppContext(stage);
        appContext.loadScreen(getClass().getResource("/fxml/login.fxml"), new LoginController());
    }

    public static void main(String[] args) {
        HibernateUtil.getSessionFactory();
        launch(args);
    }
}
