/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.vianasports;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pt.vianasports.controllers.GenericController;
import pt.vianasports.models.Cart;
import pt.vianasports.models.Employee;
import pt.vianasports.models.Product;
import pt.vianasports.services.*;

/**
 *
 * @author pedro
 */
public class AppContext {
    public final EmployeeService empService;
    public final RoleService roleService;
    public final ClientService clientService;
    public SupplierService supplierService;
    public CategoryService categoryService;

    public Employee loggedUser;
    private final Stage window;
    private final ResourceBundle strings;
    public String currentPageTitle;
    public ProductService productService;

    public Cart cart;


    public AppContext(Stage window) {
        this.currentPageTitle = "";
        this.window = window;
        window.setTitle("Viana Sports");

       this.strings = ResourceBundle.getBundle("strings");

       empService = new EmployeeService();
       roleService = new RoleService();
       clientService = new ClientService();
       supplierService = new SupplierService();
       categoryService = new CategoryService();
       productService = new ProductService();

       cart = new Cart();
    }
    
    public void loadScreen(URL location, GenericController controller) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setResources(this.strings);
        controller.setContext(this);
        fxmlLoader.setController(controller);
        fxmlLoader.setLocation(location);
        Scene scene = new Scene(fxmlLoader.load(), 1280, 720);
        scene.getStylesheets().add(getClass().getResource("/css/main.css").toExternalForm());
        window.setScene(scene);
        window.show();
    }
    
     public void loadScreen(URL location, GenericController controller, String title) throws IOException {
        this.currentPageTitle = title;
        loadScreen(location, controller);
    }
    
    public Object loadNode(URL location, GenericController controller) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setResources(this.strings);
        controller.setContext(this);
        fxmlLoader.setController(controller);
        fxmlLoader.setLocation(location);
        return fxmlLoader.load();
    }
   
}
