package pt.vianasports.models;

public class CartLine {
    ProductSize productSize;
    int quantity;

    public CartLine(ProductSize productSize, int quantity) {
        this.productSize = productSize;
        this.quantity = quantity;
    }

    public ProductSize getProductSize() {
        return productSize;
    }

    public void setProductSize(ProductSize productSize) {
        this.productSize = productSize;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
